﻿using UnityEngine;

public class CoinGenerator : MonoBehaviour
{
	public GameObject coinPrefab;

	public float cooldawn = 2f;

	private float timer;

	void Start() => timer = cooldawn;	

	void Update()
	{
		timer -= Time.deltaTime;

		if (timer < 0)
		{
			Vector3 pos = new Vector3(Random.value * 18 + 1, 20, Random.value * 18 + 1);
			Instantiate(coinPrefab, pos, Quaternion.identity);
			timer = cooldawn;
		}
	}
}
