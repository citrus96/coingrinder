﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIElement : MonoBehaviour, IPointerClickHandler
{
	private GameObject canvas;

	private Menu menu;

	private AudioSource sound;

	private Selectable element;


	void Start()
	{
		canvas = GameObject.Find("Canvas");
		menu = canvas?.GetComponent<Menu>();
		element = GetComponent<Selectable>();

		if (!gameObject.TryGetComponent(out sound))
		{
			sound = gameObject.AddComponent<AudioSource>();
			sound.clip = Resources.Load<AudioClip>("SoundClick");
		}
		
	}


	void Update()
	{
		menu.OnStartMenuAnimation += BlockElement;
		menu.OnEndMenuAnimation += ActivateElement;
	}

	private void BlockElement() => element.interactable = false;

	private void ActivateElement() => element.interactable = true;

	public void OnPointerClick(PointerEventData eventData)
	{
		if (Menu.SfxOn)
		{
			sound.Play();
		}		
	}
}
