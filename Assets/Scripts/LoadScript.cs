﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Object = UnityEngine.Object;

public class LoadScript : MonoBehaviour
{
	public static string bundleURL = "https://testapp05052021.000webhostapp.com/playgound.pak";
	public static AssetBundle bundle;

	IEnumerator LoadBundle()
	{
		while (!Caching.ready)
		{
			yield return null;
		}

		UnityWebRequest request = UnityWebRequest.Get(bundleURL + ".manifest");

		yield return request.SendWebRequest();

		if (!request.isHttpError && !request.isNetworkError)
		{
			Hash128 hash = default;

			var hashRow = request.downloadHandler.text.ToString().Split("\n".ToCharArray())[5];

			hash = Hash128.Parse(hashRow.Split(':')[1].Trim());

			if (hash.isValid == true)
			{
				request.Dispose();

				request = UnityWebRequestAssetBundle.GetAssetBundle(bundleURL, hash, 0);

				yield return request.SendWebRequest();

				if (!request.isHttpError && !request.isNetworkError)
				{
					//оффлайн режим
				}
				else
				{
					
				}
			}
			else
			{
				if (request.isHttpError && request.isNetworkError)
				{
					//нужна сеть
				}
			}
		}
		else
		{
			
		}
		request.Dispose();

		//request.SetRequestHeader("AUTHORIZTATION", auth);
		//yield return request.SendWebRequest();

		//if (request.result != UnityWebRequest.Result.Success)
		//{
		//	Debug.Log(request.error);
		//}
		//else
		//{
		//	AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(request);
		//	Object[] obj = bundle.LoadAllAssets<GameObject>();
		//	foreach (Object o in obj)
		//	{
		//		Instantiate(o);
		//	}
		//}
	}

	IEnumerator LoadAssetBundleFromServerWithCache(string url, Action<AssetBundle> response)
	{
		while (!Caching.ready)
		{
			yield return null;
		}

		UnityWebRequest request = UnityWebRequest.Get(bundleURL + ".manifest");

		yield return request.SendWebRequest();

		if (!request.isHttpError && !request.isNetworkError)
		{
			Hash128 hash = default;

			var hashRow = request.downloadHandler.text.ToString().Split("\n".ToCharArray())[5];

			hash = Hash128.Parse(hashRow.Split(':')[1].Trim());

			if (hash.isValid == true)
			{
				request.Dispose();

				request = UnityWebRequestAssetBundle.GetAssetBundle(bundleURL, hash, 0);

				yield return request.SendWebRequest();

				if (!request.isHttpError && !request.isNetworkError)
				{
					response(DownloadHandlerAssetBundle.GetContent(request));
				}
				else
				{
					response(null);
				}
			}
			else
			{
				response(null);
			}
		}
		else
		{
			response(null);
		}
		request.Dispose();
	}		
}
