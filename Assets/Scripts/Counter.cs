﻿using UnityEngine;
using UnityEngine.UI;

public  class Counter : MonoBehaviour
{
	private int CoinsCount = 0;
	private Text text;

	void Start() => text = GetComponent<Text>();
	void Update() => text.text = "Coins: " + CoinsCount.ToString();
	public void AddCoint() => CoinsCount++;
}
