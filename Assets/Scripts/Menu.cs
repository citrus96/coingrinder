﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using Image = UnityEngine.UI.Image;
using Toggle = UnityEngine.UI.Toggle;

public class Menu : MonoBehaviour
{
	public static bool MusicOn = true;
	public static bool SfxOn = true;
	public static string bundleURL = "https://testapp05052021.000webhostapp.com/playgound.pak";
	public static AssetBundle bundle;

	public Action OnStartMenuAnimation;
	public Action OnEndMenuAnimation;
	public AudioSource music;

	[SerializeField]
	private GameObject menuCanvas, settingsCanvas, background;

	[SerializeField]
	private Toggle musicToggle, sfxToggle;

	private Image img;

	private bool isShowSettings = false;

	private float speed = 1500f;

	public struct MyStruct
	{
		public string hash;
	}

	void Start()
	{
		img = background.GetComponent<Image>();

		StartCoroutine(WaitFadeOutCR(true, 2f));
	}

	// Update is called once per frame
	void Update()
	{

		if (SceneManager.GetActiveScene().name == "StartMenu")
		{
			ShowMenuOrSettings();
		}

		music.mute = !MusicOn;

	}

	public void ShowMenuOrSettings()
	{
		if (isShowSettings)
		{
			if (menuCanvas.transform.position.x > -(Screen.width / 2 - 10))
			{
				menuCanvas.transform.Translate(Vector3.left * Time.deltaTime * speed);
				settingsCanvas.transform.Translate(Vector3.left * Time.deltaTime * speed);
				OnStartMenuAnimation?.Invoke();
			}
			else
			{
				OnEndMenuAnimation?.Invoke();
			}
		}
		else
		{
			if (menuCanvas.transform.position.x < (Screen.width / 2 - 10))
			{
				menuCanvas.transform.Translate(Vector3.right * Time.deltaTime * speed);
				settingsCanvas.transform.Translate(Vector3.right * Time.deltaTime * speed);
				OnStartMenuAnimation?.Invoke();
			}
			else
			{
				OnEndMenuAnimation?.Invoke();
			}
		}

	}

	public void ShowSettings() => isShowSettings = true;

	public void ShowMenu() => isShowSettings = false;
	public void Exit() => Application.Quit();


	public void PlayButton()
	{
		StartCoroutine(GoLoadScene(false, 2f));
		img.transform.GetChild(0).gameObject.SetActive(true);
	}

	public void ToggleMusic() => MusicOn = musicToggle.isOn;

	public void ToggleSfq() => SfxOn = sfxToggle.isOn;

	public void MenuButton() => StartCoroutine(GoLoadScene(false, 1f));

	public void BackToMenu()
	{
		background.transform.GetChild(1).gameObject.SetActive(false);
		StartCoroutine(WaitFadeOutCR(true, 2f));
	}

	public void CloseWindow() => transform.GetChild(4).gameObject.SetActive(false);

	private IEnumerator GoLoadScene(bool fadeAway, float duration)
	{
		yield return WaitFadeOutCR(fadeAway, duration);

		if (SceneManager.GetActiveScene().name == "StartMenu")
		{
			StartCoroutine(LoadBundle("Playground"));
		}
		else
		{
			SceneManager.LoadScene("StartMenu");
		}
	}


	private IEnumerator WaitFadeOutCR(bool fadeAway, float duration)
	{
		if (fadeAway)
		{
			for (float i = duration; i >= 0; i -= Time.deltaTime)
			{
				img.color = new Color(img.color.r, img.color.g, img.color.b, i);
				yield return null;
			}
		}
		// fade from transparent to opaque
		else
		{
			for (float i = 0; i <= duration; i += Time.deltaTime)
			{
				img.color = new Color(img.color.r, img.color.g, img.color.b, i);
				yield return null;
			}
		}
		img.transform.GetChild(0).gameObject.SetActive(!fadeAway);
	}

	IEnumerator LoadBundle(string sceneName)
	{
		while (!Caching.ready)
		{
			yield return null;
		}

		UnityWebRequest request = UnityWebRequest.Get(bundleURL + ".manifest");

		yield return request.SendWebRequest();

		if (!request.isHttpError && !request.isNetworkError)
		{
			Hash128 hash = default;

			var hashRow = request.downloadHandler.text.ToString().Split("\n".ToCharArray())[5];

			hash = Hash128.Parse(hashRow.Split(':')[1].Trim());
			var xz = new MyStruct();
			xz.hash = hash.ToString();
			var toJson = JsonUtility.ToJson(xz, true);
			File.WriteAllText(Application.persistentDataPath + "/bundleHash.json", toJson);


			if (hash.isValid == true)
			{
				request.Dispose();

				request = UnityWebRequestAssetBundle.GetAssetBundle(bundleURL, hash, 0);

				yield return request.SendWebRequest();

				if (!request.isHttpError && !request.isNetworkError)
				{
					bundle = DownloadHandlerAssetBundle.GetContent(request);
				}
			}
		}
		else
		{
			if (File.Exists(Application.persistentDataPath + "/bundleHash.json"))
			{
				var file = File.ReadAllText(Application.persistentDataPath + "/bundleHash.json");
				var hashStruct = JsonUtility.FromJson<MyStruct>(file);
				Hash128 hash = Hash128.Parse(hashStruct.hash);
				if (hash.isValid == true)
				{
					request.Dispose();

					request = UnityWebRequestAssetBundle.GetAssetBundle(bundleURL, hash, 0);

					yield return request.SendWebRequest();

					bundle = DownloadHandlerAssetBundle.GetContent(request);
				}
			}
		}

		if (bundle != null)
		{
			SceneManager.LoadScene(sceneName);
		}
		else
		{
			background.transform.GetChild(1).gameObject.SetActive(true);
		}
		request.Dispose();
	}
}


