﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Coin : MonoBehaviour
{
	public int LifeTime = 5;
	public float idleHeigh = 5f;
	public float fallingSpeed = 1f;

	private AudioSource sound;
	private string test1 = "rebase!";
	private string test2 = "rebase?";

	private string test3 = "yes...";
	private Counter coinCounter;

	void Start()
	{
		sound = GetComponent<AudioSource>();
		var obj = GameObject.Find("CoinsCounter");
		coinCounter = obj?.GetComponent<Counter>();
	}

	void Update()
	{
		transform.Rotate(Vector3.down,0.5f,Space.Self);

		if (transform.position.y <= idleHeigh)
		{
			StartCoroutine(WaitForRemove());
		}
		else
		{
			transform.Translate(Vector3.down*0.1f*fallingSpeed,Space.World);
		}
	}

	IEnumerator WaitForRemove()
	{
		yield return new WaitForSeconds(LifeTime);
		Destroy(gameObject);
	}

	void OnCollisionEnter(Collision collision)
	{
		if (collision.collider.CompareTag("Player"))
		{
			if(Menu.SfxOn)
				sound.Play();
			coinCounter.AddCoint();
			GetComponent<Collider>().enabled = false;
			Destroy(gameObject,0.5f);
		}
	}
}
