﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class PlaygroundLoader : MonoBehaviour
{
	void Start()
	{	
		if (Menu.bundle != null)
		{
			GameObject[] obj = Menu.bundle.LoadAllAssets<GameObject>();
			foreach (GameObject o in obj)
			{
				Instantiate(o);
			}

			StartCoroutine(CheckInternetConnection((isConnected) =>
			{
				if (!isConnected)
				{
					GameObject root = GameObject.Find("InBundle(Clone)");

					root.transform.GetChild(2).GetChild(4).gameObject.SetActive(true);	
				}
			}));
		}
	}

	IEnumerator CheckInternetConnection(Action<bool> action){
		var www = new UnityWebRequest("http://google.com");
		yield return www.SendWebRequest();
		if (www.result == UnityWebRequest.Result.ConnectionError) {
			action (false);
		} else {
			action (true);
		}
	} 
}
